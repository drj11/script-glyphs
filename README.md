# glyphs-script


## Count nodes in each glyph and sort by count

```
nodeCounts = []
for g in Glyphs.font.glyphs:
	nodeCounts.append( (g.name, sum([len(p.nodes) for p in g.layers[0].paths])) )
for n in (sorted(nodeCounts, key=lambda n: n[1])):
	print(n)
```

## Set the text of the current tab

```
font = Glyphs.font
tab = font.currentTab or font.newTab()
text = "Greetings, Earthlings!"
tab.graphicView().setDisplayString_(text)

```

# END
