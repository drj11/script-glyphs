# MenuTitle: Spaced

print(f"""{__file__}: A Glyphs script by David Jones <drj@pobox.com>""")
# Consider improving me at https://gitlab.com/drj11/script-glyphs

"""
This Spaced script generates (in the current tab) a bunch of
spacing strings of the form HH?HH.
It has only been tested with Latin fonts.

Generally the script tries to be "smart" about what glyphs are
used to space between and there are a fair number of special
cases that i don't summarise. Given that:

- Capital letters are spaced between /H
- Lowercase letters are spaced between /n
- Smallcaps letters are spaced between /h.smcp
- Numbers and maths operators are spaced between /zero, except
- Inferior numbers are spaced between /H
- Smallcaps numbers are spaced between /zero.smcp
- Punctuation is spaced between both /H and /n, except
- `.case` punctuation is spaced only between /H
"""

# todo
# - operators like u+2A6A TILDE OPERATOR WITH DOT ABOVE should
#   probably be spaced between /zero

# https://docs.python.org/3.7/library/itertools.html
import itertools


def exported(gs):
    """Return only exported glyphs."""
    return [g for g in gs if g.export]


def setTabText(tab, text):
    tab.graphicView().setDisplayString_(text)


def slashQuote(name):
    """/quote a name, if necessary."""
    if len(name) <= 1:
        return name
    return "/" + name + " "


# Numbers, spaced betweens /zero s. "zero. This is the model
#   character most type designers use to space the rest of these
#   groups. They are used in chains to test how the current
#   character spaces between other straight or round stems." from
#   https://learn.microsoft.com/en-us/typography/develop/character-design-standards/figures
# Inferior Numbers, spaced between H.
# Math Symbols, spaced between 0. This is good for a lot of operators,
#  like > and ≈, but not so good for ∫ and similar.
def sides(gl):
    """For a given glyph, return a list of strings to be
    used at its side.
    When left- and right- strings are different
    a | separator is used between them in a string: "LEFT|RIGHT".
    """
    if g.category not in ["Letter", "Number", "Punctuation", "Symbol"]:
        return []

    if g.name == "fraction":
        return ["/zero.numr /zero.numr |/zero.dnom /zero.dnom "]
    if g.name == "Ldot":
        return ["HH|LL"]
    if g.name == "ldot":
        return ["nn|ll"]
    if g.name == "onefraction":
        return ["/zero /zero |/zero.dnom /zero.dnom "]

    if g.name in ["degree", "centigrade", "fahrenheit", "kelvin"]:
        return ["99"]

    if g.category == "Letter":
        if g.case == GSLowercase:
            return ["nn"]
        if g.case == GSSmallcaps:
            return [f"{hsc} {hsc} "]
    if g.category == "Number":
        if g.name.endswith("inferior"):
            return ["HH"]
        if g.case == GSSmallcaps:
            return [f"{zerosc} {zerosc} "]
        return ["00"]
    if g.category == "Punctuation":
        if g.name.endswith(".case"):
            return ["HH"]
        return ["HH", "nn"]
    if g.category == "Symbol":
        if g.subCategory in ["Math", "Currency"]:
            if g.case == GSSmallcaps:
                return [f"{zerosc} {zerosc} "]
            return ["00"]
        if g.case == GSSmallcaps:
            return [f"{hsc} {hsc} "]
        return ["HH"]

    return ["HH"]


font = Glyphs.font

# string for smallcap h; could be /h.smcp or /h.sc
if font.glyphs["h.smcp"]:
    hsc = "/h.smcp"
else:
    hsc = "/h.sc"

# string for smallcap zero
if font.glyphs["zero.smcp"]:
    zerosc = "/zero.smcp"
else:
    zerosc = "/zero.sc"

cats = []
# Group by category, so that we can put a divider ("\n") in.
for cat, glyphs in itertools.groupby(exported(font.glyphs), lambda g: g.category):
    frags = []
    # print("========== cat", cat)
    for g in glyphs:
        # print(cat, ",", g.subCategory, "case", g.case, g)
        for side in sides(g):
            l, r = (side.split("|") * 2)[:2]
            frag = l + slashQuote(g.name) + r
            frags.append(frag)
    catfrag = " ".join(frags)
    cats.append(catfrag)

text = "\n".join(cats)

tab = font.currentTab or font.newTab()

setTabText(tab, text)
