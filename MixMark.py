# MenuTitle: MixMark

print(f"""{__file__}: A Glyphs script by David Jones <drj@pobox.com>""")
# Consider improving me at https://gitlab.com/drj11/script-glyphs

"""
This MixMark script edits the text in the current tab
to select a random mark (diacritic) for each glyph where
that is possible.
The glyphs themselves are not edited, nor are new ones created.
For example if you have not added the /vtilde glyph then
it will not be used in the text, even though /v and /tildecomb
exist.
It has only been tested with Latin fonts.

For each glyph in the text:
- if this glyph has no components, and is used as a component in another,
  one of those glyphs is used (add a mark)
- if this glyph has two or more components and at least one
  that can be changed to yield an existing other glyph,
  one of those is selected at random.
"""

# https://docs.python.org/3.8/library/collections.html
import collections

# https://docs.python.org/3.7/library/itertools.html
import itertools

# https://docs.python.org/3.8/library/random.html
import random


def exported(gs):
    """Return only exported glyphs."""
    return [g for g in gs if g.export]


def setTabText(tab, text):
    tab.graphicView().setDisplayString_(text)


def slashQuote(name):
    """/quote a name, if necessary."""
    if len(name) <= 1:
        return name
    return "/" + name + " "


def PrepareCNames(font):
    """The result is a dict() `d` of all composite glyphs in all layers.
    The dict() has two levels of keys,
    first for the master, second for the glyph name:
    d[m][g] is a _set_ of the names of the components for
    the glyph named `g` in the master `m`.
    """

    d = collections.defaultdict(dict)
    for g in font.glyphs:
      for l in g.layers:
        names = set(c.name for c in l.components)
        if not names:
            continue
        d[l.master][g.name] = names
    return d
    print(d.keys())
    print(d)


def Nearby(cs, d):
    """`cs` should be a set of component names;
    from the dict() `d` the set of glyphs that have
    nearly the same components are returned.
    The dict() `d` should map from glyph names to sets of component
    names.
    """
    l = []
    for gname, cnames in d.items():
        if len(cs - cnames) == 1 and len(cnames - cs) == 1:
            l.append(gname)
    return l


def Within(component, d):
    """`component` should be a single glyph name;
    all the glyphs that feature have `component` as a component
    are returned.
    """

    return [g for g,cs in d.items() if component in cs]
        

def ScrobbleTabText(tab, fcnames):
    """
    `tab` is GSEditViewController https://docu.glyphsapp.com/#gseditviewcontroller
    typically obtained from font.currentTab.

    `fcnames` (font component names) should be the output of PrepareCNames().
    """

    # The replacement layer instances
    replacements = []

    for layer in tab.layers:
        # set of component names
        cnames = set(c.name for c in layer.components)
        # This "if" catches glyphs that do not have components,
        # like /a, and also glyphs that only have one component,
        # like /onesuperior (which in Airvent is a shifted
        # /one.smol).
        # The latter group includes glyphs like /cent
        # which is a single component, /c, plus a drawing.
        # Possibly that's wrong, but for now it is what it is.
        # /cent is a dead end: with the current algorithm, it
        # will not be changed (/pertenthousand is another one).
        if len(cnames) <= 1:
            gs = Within(layer.parent.name, fcnames[layer.master])
        else:
            gs = Nearby(cnames, fcnames[layer.master])
        print(layer, gs)
        winner = None
        if gs:
            winner = random.choice(gs)
        if winner:
            lid = layer.master.id
            l = font.glyphs[winner].layers[lid]
            replacements.append(l)
        else:
            # Handles newline (in the GSControlLayer) and
            # glyphs that are neither composite, nor appear
            # as components.
            replacements.append(layer)
    print(replacements)
    tab.layers = replacements


font = Glyphs.font
cnames = PrepareCNames(font)

tab = font.currentTab
if tab:
    ScrobbleTabText(tab, cnames)

print(f"""{__file__}: END""")
