# MenuTitle: KPair

print(f"""{__file__}: A Glyphs script by David Jones <drj@pobox.com>""")
# Consider improving me at https://gitlab.com/drj11/script-glyphs

"""
This KPair script edits the selected text in the current tab,
replacing it with a sequence of complete pairs,
where each pair consists of one item from the first row
of the selection and one item from the second row of the
selection.
If the selection only has one row, it is paired with itself.
"""


def PairSelectedTabText(tab):
    """
    `tab` is GSEditViewController https://docu.glyphsapp.com/#gseditviewcontroller
    typically obtained from font.currentTab.
    """

    tab.textCursor
    if tab.textRange == 0:
        print("Please select two rows of text")
        return

    selected = tab.layers[tab.textCursor : tab.textCursor + tab.textRange]

    # Collect up to two rows in selection
    row = [[], []]
    index = 0
    for l in selected:
        if isinstance(l, GSControlLayer):
            index += 1
            if index >= len(row):
                break
            continue
        row[index].append(l)

    # Make pairs with self if only one row
    if len(row[1]) == 0:
        row[1] = row[0]

    # Make all pairs
    out = []
    for a in row[0]:
        for b in row[1]:
            out.extend([a, b])
        out.append(a)
        # Approved newline https://docu.glyphsapp.com/#GSEditViewController.layers
        out.append(GSControlLayer(10))

    replacements = out
    tab.layers = (
        tab.layers[: tab.textCursor]
        + replacements
        + tab.layers[tab.textCursor + tab.textRange :]
    )

    tab.textRange = len(replacements)


font = Glyphs.font

tab = font.currentTab
if tab:
    PairSelectedTabText(tab)

print(f"""{__file__}: END""")
